c straightline3.f
c Least-squares fit a straight line y=A+Bx to a set of data points.
c The intercept A is an input fixed quantity, so only B is solved for.
c Returns estimates for B and sig(B) (and given A).
c Results are also given when all the sig(y)'s are increased by a 
c constant amount such that chisqn=1
c The data must be given in an ascii file in the form, x, y, sig(y).
c JKW, Mosman 21/5/03
c f77 -o straightline3 straightline3.f

      implicit none
      integer arraysize
      parameter(arraysize=1024)
      real*4 x(arraysize), y(arraysize), sigy(arraysize)
      real*4 a,b,s,sx,sy,sxx,sxy,siga,sigb
      real*4 chisq,chisqn,sigystep,avsigy
      integer npts,i,niter
      character*64 infile

 21   WRITE(*,'(a)') 'Input data file? (in)'
      infile=' '
      READ(*,'(a)') infile
      if (infile(1:1).eq.' ') infile='in'
 4    OPEN(unit=1,file=infile,status='old',err=21)

      write(*,*)' Value of intercept (A)? '
      read(5,*)a

c The input file infile must have the form: x  y  sig(y)
      i=1
 1    READ(1,*,err=2000,end=2) x(i),y(i),sigy(i)
      i=i+1
      goto 1

 2    close(1)
      npts=i-1

c      do i=1,npts
c         write(*,*)x(i),y(i),sigy(i)
c      end do

      avsigy=0.0
      do i=1,npts
         avsigy = avsigy + sigy(i)
      end do
      avsigy = avsigy/real(npts)
c sigystep is the initial value of the constant added to the sigy(i)'s
c taken as 1/20th of the mean sigy
      sigystep = avsigy/20.0

      niter=1

 5    s=0.0
      sx=0.0
      sxx=0.0
      sy=0.0
      sxy=0.0

      do i=1,npts
         s   = s    + 1/(sigy(i)**2)
         sx  = sx   + x(i)/(sigy(i)**2)
         sy  = sy   + y(i)/(sigy(i)**2)
         sxx = sxx  + (x(i)**2)/(sigy(i)**2)
         sxy = sxy  + (x(i)*y(i))/(sigy(i)**2)
      end do

      b = (sy - a*s)/sx
      sigb = (s**0.5)/sx

      if(niter.eq.1)then
         write(*,*)' Values for unmodified errors:'
         write(*,*)' Parameters for least-squares fit y = A + Bx: '
         write(*,*)' B, sig(B) = ', b,sigb
         write(*,*)' Fixed A = ', a
      end if

      chisq = 0.0
      chisqn = 0.0
      do i=1,npts
         chisq = chisq + ((y(i) - a - b*x(i))/sigy(i))**2
      end do
      chisqn = chisq/real(real(npts)-1)

c      write(*,*)' chisq =', chisq
      if(chisqn.le.1.0)goto 3000
      do i=1,npts
         sigy(i) = sigy(i) + sigystep
      end do
      niter=niter+1
      goto 5

 3000 write(*,*)' Values for modified error, sigy(i) + ',sigystep
      write(*,*)' Parameters for least-squares fit y = A + Bx: '
      write(*,*)' B, sig(B) = ', b,sigb
      write(*,*)' Fixed A = ', a

 2000 stop
      end
